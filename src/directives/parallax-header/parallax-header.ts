import { Directive, ElementRef, Renderer2, Input } from '@angular/core';
import { DomController } from 'ionic-angular';

@Directive({
	selector: '[parallaxHeader]',
	host: {
		'(ionScroll)': 'onContentScroll($event)'
	}
})
export class ParallaxHeaderDirective {

	@Input('parallaxHeader') imagePath: string;
	@Input('parallaxHeight') parallaxHeight: string;

	headerHeight: any;
	header: any;

	constructor(private element: ElementRef, private renderer: Renderer2, private domCtrl: DomController) {

	}

	ngAfterViewInit(){

		this.headerHeight = this.parallaxHeight;

		this.domCtrl.write(() => {

			let scrollContent = this.element.nativeElement.querySelector('.scroll-content');
			this.header = this.renderer.createElement('div');

			this.renderer.insertBefore(this.element.nativeElement, this.header, scrollContent);
			
			this.renderer.setStyle(scrollContent, 'padding-top', this.headerHeight + 'px');

			this.renderer.setStyle(this.header, 'margin-top', '44px');
			this.renderer.setStyle(this.header, 'background-image', 'url(' + this.imagePath + ')');
			this.renderer.setStyle(this.header, 'height', this.headerHeight + 'px');
			this.renderer.setStyle(this.header, 'background-size', 'cover');

		});

	}

	onContentScroll(ev){

		let translateAmt;
		let scaleAmt;

        if(ev.scrollTop >= 0){
            translateAmt = -(ev.scrollTop / 4);
            scaleAmt = 1;
        } else {
            translateAmt = 0;
            scaleAmt = -ev.scrollTop / this.headerHeight + 1;
        }

		this.domCtrl.write(() => {
			this.renderer.setStyle(this.header, 'transform', 'translate3d(0,'+translateAmt+'px,0) scale('+scaleAmt+','+scaleAmt+')');
		});

	}

}
